package timotei;

import java.io.IOException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class Main extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("MapScene.fxml"));
        
        Scene scene = new Scene(root);
      
      
        stage.setScene(scene);
        scene.getStylesheets().add(Main.class.getResource("stylesheet.css").toExternalForm());
        stage.show();
        stage.setResizable(false);
        
        stage.setOnCloseRequest((WindowEvent event) -> {
            // Write logs
            writeLogs();
            
            // Save objects
            saveObjects();
            
            // Close all windows when main window is closed
            Platform.exit();
        });
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
    private void writeLogs() {
        
        try {
            Log.makeLog();
        } catch (IOException ex) {
            // well shit... No way to show error to user anymore...
            System.out.println("Creating logs failed for unknown reason.");
            System.out.println(ex);
        }
        
    }
    
    private void saveObjects() {
        try {
            PackageSerializer.storeAllPackages();
        } catch (IOException ex) {
            System.out.println("Couldn't store objects");
            System.out.println(ex);
        }
    }
}
