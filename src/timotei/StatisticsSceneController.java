/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timotei;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author matias
 */
public class StatisticsSceneController implements Initializable {

    @FXML
    private ListView<StatisticsFile> LogDateTimeList;
    @FXML
    private ListView<PostPackage> packageSelector;
    @FXML
    private Label PackageInformationLabel;
    
    private final DateFormat humanReadableDate = new SimpleDateFormat("dd.MM.yyyy HH:mm");

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // Init datetime selector
        for (File f : FileIO.getFilesInFolder("storage", ".ser")) {
            try {
                
                // Add log entry to list
                Date date = decodeFilenameDate(f.getName());
                StatisticsFile statFile = new StatisticsFile(f, date);
                LogDateTimeList.getItems().add(statFile);
            } catch (ParseException ex) {
                System.out.println("file name not valid");
            }
        }
    }    

    @FXML
    private void dateTimeSelected(MouseEvent event) {
        // update package list for packages of selected log entry
        StatisticsFile selected = LogDateTimeList.getSelectionModel().getSelectedItem();
        
        if (selected != null) {
            try {
                ArrayList<PostPackage> datePackages = PackageSerializer.loadAllPackages(selected.getDate());
                packageSelector.getItems().clear();
                packageSelector.getItems().addAll(datePackages);
                
            } catch (IOException ex) {
                System.out.println("File not found");
                System.out.println(ex);
            }
        }
        
        PackageInformationLabel.setText("");
    }

    @FXML
    private void packageSelected(MouseEvent event) {
        PostPackage pp = packageSelector.getSelectionModel().getSelectedItem();
        if (pp != null) {
            PackageInformationLabel.setText(pp.getPackageClass() + ". luokan paketti\n\n"
                    + "Sisältö: " + pp.getItem().getName() + "\n"
                    + "Mistä: " + pp.getSendSmartPost() + "\n"
                    + "Minne: " + pp.getReceiveSmartPost() + "\n"
                    + "Tavaran koko: " + pp.getItem().getX() + "*" + pp.getItem().getY() + "*" + pp.getItem().getZ() + " cm\n"
                    + "Paino: " + pp.getItem().getWeight() + " kg\n"
                    + "Matkan pituus: " + pp.getTravelDistance() + " km\n"
                    + "Särkyvää: " + (pp.getItem().isBreakable() ? "Kyllä" : "Ei") + "\n"
                    + "Lähetetty: " + (pp.isSent() ? "Kyllä" : "Ei") + "\n"
                    + "Hajonnut: " + (pp.getItem().isBroken() ? "Kyllä" : "Ei") + "\n"
                    + "Huom: " + (pp.isTooFarAway() ? "Pakettia ei voitu lähettää. Matka on liian pitkä" : "Ei huomioita"));
        }
        
    }
    
    private Date decodeFilenameDate(String filename) throws ParseException {
        return Log.LOGFILEDATEFORMAT.parse(filename.replaceAll(PackageSerializer.PACKAGE_STORAGE_POSTFIX, ""));
    }
    
    private String encodeFilenameDate(Date date) {
        return Log.LOGFILEDATEFORMAT.format(date);
    }
}
