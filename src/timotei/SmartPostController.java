
package timotei;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author matias
 */
public class SmartPostController {
    private static SmartPostController instance = null;
    
    private final ArrayList<SmartPost> smartPosts;
    
    private SmartPostController() {
        smartPosts = new ArrayList<>();
        loadSmartPosts();
    }
    
    public static SmartPostController getInstance() {
        if (instance == null) {
            instance = new SmartPostController();
        }
        return instance;
    }
    
    private void loadSmartPosts() {
        try {
            URL url = new URL("http://smartpost.ee/fi_apt.xml");
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(url.openStream());
            
            NodeList places = doc.getElementsByTagName("place");
            for (int i = 0; i < places.getLength(); i++) {
                Node n = places.item(i);
                if (n.getNodeType() == Node.ELEMENT_NODE) {
                    Element e = (Element) n;
                    String name = e.getElementsByTagName("postoffice").item(0).getTextContent();
                    String city = e.getElementsByTagName("city").item(0).getTextContent().toUpperCase();
                    String code = e.getElementsByTagName("code").item(0).getTextContent();
                    String address = e.getElementsByTagName("address").item(0).getTextContent();
                    String availability = e.getElementsByTagName("availability").item(0).getTextContent();
                    float lat = Float.parseFloat(e.getElementsByTagName("lat").item(0).getTextContent());
                    float lng = Float.parseFloat(e.getElementsByTagName("lng").item(0).getTextContent());
                    
                    smartPosts.add(new SmartPost(name, city, address, code, availability, lat, lng));
                }
            }
            
        } catch (MalformedURLException ex) {
            Logger.getLogger(SmartPostController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(SmartPostController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public ArrayList<SmartPost> getSmartPosts(String city) {
        ArrayList<SmartPost> found = new ArrayList<>();
        for (SmartPost smartPost : smartPosts) {
            if (smartPost.getCity().equals(city)) {
                found.add(smartPost);
            }
        }
        return found;
    }

    public ArrayList<SmartPost> getSmartPosts() {
        return smartPosts;
    }
    
    public ArrayList<String> getCitiesWithSmartPost() {
        ArrayList<String> cities = new ArrayList<>();
        
        for (SmartPost sp : smartPosts) {
            if (!cities.contains(sp.getCity())) {
                cities.add(sp.getCity());
            }
        }
        
        return cities;
    }
    
    
}
