/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timotei;

/**
 *
 * @author HessuMcMillan
 */
public class PackageInformation {
    private String info;
    
    public String getPacketInfo(){
       info = "1. luokan paketti\n"
               + "kaikista nopein pakettiluokka.\n"
               + "Paketit saattavat mennä rikki matkalla.\n"
               + "Maksimi matka: 150 km päähän.\n"
               + "Paketin maksimi koko: 60cm*70cm*40cm\n\n"
               + "2. luokan paketti\n kestävät parhaiten kaiken särkyvän tavaran kuljettamisen.\n"
               + "Maksimi koko: 50cm*50cm*30cm\n\n"
               + "3. luokan paketti\n"
               + "Maksimi koko: 60cm*60cm*60cm\n";


        return info;
    }
}
