
package timotei;

import java.io.Serializable;



/**
 *
 * @author matias
 */
public class Item implements Serializable {
    private final boolean breakable;
    private boolean broken = false;
   
    
    /* Item dimensions */
    private final double sizeX;
    private final double sizeY;
    private final double sizeZ;
    private final double weight;
    
    private final String name;
    
    
    public Item(boolean breakable, double sizeX, double sizeY, double sizeZ, double weight, String name) {
     
        this.breakable = breakable;
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.sizeZ = sizeZ;
        this.weight = weight;
        this.name = name;
        
    }
    

    public double getZ() {
        return sizeZ;
    }

    public double getY() {
        return sizeY;
    }

    public double getX() {
        return sizeX;
    }

    public double getWeight() {
        return weight;
    }

    public boolean isBreakable() {
        return breakable;
    }

    public boolean isBroken() {
        return broken;
    }

    public void breakItem() {
        System.out.println("Item " + this.name + " has been broken in transport.");
        this.broken = true;
    }

    public double getVolume() {
        return sizeX * sizeY * sizeZ;
    }
    public String getName() {
        return name;
    }
    @Override
    public String toString(){
        return this.name;
    }
   
}
