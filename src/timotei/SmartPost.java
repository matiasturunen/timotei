
package timotei;

import java.io.Serializable;

/**
 *
 * @author matias
 */
public class SmartPost implements Serializable {
    private final String name;
    private final String city;
    private final String address;
    private final String code;
    private final String availability;
    private final float lat;
    private final float lng;
    private boolean inMap = false;

    public SmartPost(String name, String city, String address, String code, String availability, float lat, float lng) {
        this.name = name;
        this.city = city;
        this.address = address;
        this.code = code;
        this.lat = lat;
        this.lng = lng;
        this.availability = availability;
    }

    @Override
    public String toString() {
        return this.city + ", " + this.name;
    }

    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }

    public String getCode() {
        return code;
    }

    public float getLat() {
        return lat;
    }

    public float getLng() {
        return lng;
    }

    public String getName() {
        return name;
    }

    public boolean isInMap() {
        return inMap;
    }

    public String getAvailability() {
        return availability;
    }

    public void setInMap(boolean inMap) {
        this.inMap = inMap;
    }
}
