/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timotei;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author HessuMcMillan
 */
public class PackageStorage implements Serializable {

    private static PackageStorage storage = null;

    private ArrayList<PostPackage> packetList = new ArrayList<>();

    public static PackageStorage getInstance() {
        if (storage == null) {
            storage = new PackageStorage();
        }
        return storage;
    }

    public ArrayList<PostPackage> getPacketList() {
        return packetList;
    }

    public void addPacket(PostPackage packet) {
        packetList.add(packet);
    }

    public void removePacket(PostPackage packet) {
        packetList.remove(packet);
    }

    public void clearPacketList() {
        packetList.clear();
    }

    public int getPacketCount() {
        return packetList.size();
    }
    
    /* Find all packages that contain item with specific name */
    public ArrayList<PostPackage> findPackage(String name) {

        ArrayList<PostPackage> packageList = new ArrayList<>();

        for (PostPackage packet : packetList) {
            if (packet.getItem().getName().equals(name)) {
                packageList.add(packet);
            }
        }
        return packageList;
    }
    
    /* Get all unsent packages */
    public ArrayList<PostPackage> getUnsentPackages() {
        ArrayList<PostPackage> unsent = new ArrayList<>();
        
        for (PostPackage pp : packetList) {
            if (!pp.isSent()) {
                unsent.add(pp);
            }
        }
        
        return unsent;
    }
    
    /* Get all sent packages */
    public ArrayList<PostPackage> getSentPackages() {
        ArrayList<PostPackage> sent = new ArrayList<>();
        
        for (PostPackage pp : packetList) {
            if (pp.isSent()) {
                sent.add(pp);
            }
        }
        
        return sent;
    }
}
