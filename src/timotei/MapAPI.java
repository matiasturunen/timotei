
package timotei;

import javafx.scene.web.WebView;

/**
 *
 * @author matias
 */
public class MapAPI {
    public static void addSmartPost(SmartPost sp, WebView map) {
        addSmartPost(sp, map, "red");
    }
    
    public static void addSmartPost(SmartPost sp, WebView map, String color) {
        String address = sp.getAddress() + ", " + sp.getCode() + " " + sp.getCity();
        String script = "document.goToLocation('" + address + "', '" + sp.getName() + "<br>" + sp.getAvailability() + "', '" + color + "')";
        map.getEngine().executeScript(script);
    }
    
    public static int createPath(SmartPost start, SmartPost end, WebView map, Integer packageClass) {
        return createPath(start, end, map, packageClass, "blue");
    }
    
    public static int createPath(SmartPost start, SmartPost end, WebView map, Integer packageClass, String color) {
        String fromTo = "[" + start.getLat() + ", " + start.getLng() + ", " + end.getLat() + ", " + end.getLng() + "]";
        String script = "document.createPath(" + fromTo + ", '" + color + "', " + packageClass + ")";
        
        // typecasting is great
        return (int) Math.round((double)map.getEngine().executeScript(script));
    }
    
    public static void deletePaths(WebView map) {
        map.getEngine().executeScript("document.deletePaths()");
    }
    
    public static void removeAllMarkers(WebView map) {
        map.getEngine().executeScript("document.removeAllMarkers()");
    }
    
    public static int getDistanceBetweenPoints(SmartPost start, SmartPost end, WebView map) {
        String script = "document.getDistanceBetweenPoints(" + start.getLat() + ", " + start.getLng() + ", " + end.getLat() + ", " + end.getLng() + ")";
        return (int) Math.round((double)map.getEngine().executeScript(script));
    }
}
