
package timotei;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author matias
 */
public class StatisticsFile {
    private final File file;
    private final Date date;
    
    private SimpleDateFormat humansFormat = new SimpleDateFormat("\"dd.MM.yyyy HH:mm\"");

    public StatisticsFile(File file, Date date) {
        this.file = file;
        this.date = date;
    }

    public File getFile() {
        return file;
    }

    public Date getDate() {
        return date;
    }
    
    public String formatDate(DateFormat format) {
        return format.format(date);
    }

    public StatisticsFile() {
        this.file = null;
        this.date = null;
    }

    @Override
    public String toString() {
        return formatDate(humansFormat);
    }
    
    
}
