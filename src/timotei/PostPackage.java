package timotei;

import java.io.Serializable;

public abstract class PostPackage implements Serializable {

    /* Flag to determine if contents of this package will break on transport */
    protected boolean willBreak = false;

    /* Box dimensions */
    private final double sizeX;
    private final double sizeY;
    private final double sizeZ;

    /* Travel packageClass. 1=Fast, 2=Average, 3=Slow */
    protected int packageClass = 1;
    
    /* Maximum weight of package, measured in kg */
    protected double maxWeight = 30;

    /* Item in this package */
    private Item item;

    /* Has this package been sent already? */
    private boolean isSent = false;
    
    /* sender and receiver smartpost */
    private SmartPost sendSmartpost;
    
    private SmartPost receiveSmartpost;
    
    private int travelDistance;
    
    private boolean tooFarAway = false;
    
    public PostPackage(double sizeX, double sizeY, double sizeZ) {
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.sizeZ = sizeZ;
        this.travelDistance = 0;
    }

    /* Puts item in the box if it fits. */
    public boolean setItem(Item item) {
        if (this.testFit(item)) {
            this.item = item;
            return true;
        }
        return false;
    }

    public void setTravelDistance(int travelDistance) {
        this.travelDistance = travelDistance;
    }

    public int getTravelDistance() {
        return travelDistance;
    }

    /* Rotate item in all directions and see if it fits */
    private boolean testFit(Item i) {
        double ix = i.getX();
        double iy = i.getY();
        double iz = i.getZ();
        double t;
        for (int j = 0; j < 5; j++) {   // don't know if it actually works...
            if (ix <= sizeX) {
                if (iy <= sizeY) {
                    if (iz <= sizeZ) {
                        return true;
                    }
                }
            }
            t = ix;
            ix = iy;
            iy = iz;
            iz = t;
        }

        return false;
    }
    
    protected void setIsTooFarAway() {
        tooFarAway = true;
    }
    
    protected void setSent() {
        isSent = true;
    }

    public boolean isTooFarAway() {
        return tooFarAway;
    }

    /* Sends this package */
    public void send() throws PackageException {
        if (this.isSent) {
            System.out.println("Package cant be sent twice!");
            throw new PackageException("Package cant be sent twice!");
        } else {
            this.isSent = true;
            
            /* Break items if possible */
            if (this.item.isBreakable()) {
                if (willBreak) {
                    System.out.println("willbreak");
                    this.item.breakItem();
                }
            }
        }
    }
    
    public void setSendSmartPost(SmartPost smartpost){
        sendSmartpost = smartpost;
    }
    
    public void setReceiveSmartPost(SmartPost smartpost){
        receiveSmartpost = smartpost;
    }
    
    public SmartPost getSendSmartPost(){
        return sendSmartpost;
    }
    
    public SmartPost getReceiveSmartPost(){
        return receiveSmartpost;
    }
    
    public double getSizeZ() {
        return sizeZ;
    }

    public double getSizeY() {
        return sizeY;
    }

    public double getSizeX() {
        return sizeX;
    }

    public Item getItem() {
        return item;
    }

    public double getMaxWeight() {
        return maxWeight;
    }

    public boolean isSent() {
        return isSent;
    }

    public int getPackageClass() {
        return packageClass;
    }

    @Override
    public String toString() {
        return item.getName() + ", Luokka " + packageClass + ", " + sendSmartpost.getCity() + " - " + receiveSmartpost.getCity();
    }
    
    
    
}
