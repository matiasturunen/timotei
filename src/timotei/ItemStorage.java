/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timotei;

import java.util.ArrayList;

/**
 *
 * @author HessuMcMillan
 */

public class ItemStorage {
     private static ArrayList<Item> itemList = new ArrayList<>();
     private static ItemStorage storage = null;
     
     public static ItemStorage getInstance(){
         if(storage == null){
            storage = new ItemStorage();
            itemList.add(new AntiqueVase());
            itemList.add(new Book());
            itemList.add(new BarbieDoll());
            itemList.add(new BowlingBall());
         }

         return storage;
     }
    
     public void addItem(Item item){
         itemList.add(item);
         System.out.println(itemList);
     }
     public ArrayList<Item> getItemList(){
        return itemList;
     }
} 
