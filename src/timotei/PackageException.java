
package timotei;

/**
 *
 * @author matias
 */
public class PackageException extends Exception {

    public PackageException(String message) {
        super(message);
    }
    
}
