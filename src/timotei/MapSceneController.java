package timotei;

import java.awt.Color;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.EventListener;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class MapSceneController implements Initializable {

    private SmartPostController spc;
    private PackageStorage packageStorage;

    @FXML
    private WebView webmap;
    @FXML
    private ComboBox<SmartPost> smartPostComboBox;
    @FXML
    private Button addSmartPostButton;
    @FXML
    private Pane addSmartpostPane;
    @FXML
    private ComboBox<String> cityComboBox;
    @FXML
    private ListView<PostPackage> packageQueue;
    @FXML
    private Pane sendPacketPane;
    @FXML
    private Label packageCountLabel;

    private final ArrayList<Tooltip> tooltips = new ArrayList<>();
    @FXML
    private ListView<PostPackage> sentPackagesList;
    @FXML
    private Label sentPackagesCountLabel;
    @FXML
    private SplitPane mapSplitpane;
    @FXML
    private SplitPane ButtonsSplitpane;
    
    
    private Stage statisticStage = null;
    private Stage packageStage = null;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        /* Add map */
        webmap.getEngine().load(getClass().getResource("index.html").toExternalForm());

        spc = SmartPostController.getInstance();
        packageStorage = PackageStorage.getInstance();
        initCitySelector();

    }

    private void initCitySelector() {
        cityComboBox.getItems().clear();
        cityComboBox.getItems().addAll(spc.getCitiesWithSmartPost());
        updateSmartPostComboBox();
    }

    private void updateSmartPostComboBox() {
        smartPostComboBox.getItems().clear();
        if (cityComboBox.getValue() == null) {
            // No city selected, smartpost picker should be empty
        } else {
            System.out.println(cityComboBox.getValue());
            smartPostComboBox.getItems().addAll(spc.getSmartPosts(cityComboBox.getValue()));
        }
    }

    @FXML
    private void citySelected(Event event) {
        updateSmartPostComboBox();
    }

    @FXML
    private void addSmartPostToMap(ActionEvent event) {
        if (smartPostComboBox.getValue() == null) {
            System.out.println("valitse SmartPost");
        } else {
            SmartPost sp = smartPostComboBox.getValue();

            if (!sp.isInMap()) {
                MapAPI.addSmartPost(sp, webmap);
                sp.setInMap(true);
            } else {
                System.out.println("SmartPost on jo kartalla");
            }

        }
    }

    @FXML
    private void openPackageWindow(ActionEvent event) throws IOException {
        // Allow only one package window at a time
        if (packageStage == null) {
            Parent packet = FXMLLoader.load(getClass().getResource("PacketScene.fxml"));
            packageStage = new Stage();
            Scene scene = new Scene(packet);
            packageStage.setResizable(false);
            packageStage.setScene(scene);
            scene.getStylesheets().add(Main.class.getResource("stylesheet.css").toExternalForm());
            
            // Update package queue when lost focus on adding window
            packageStage.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                updatePackageQueue();
                updateSentPackageList();
                System.out.println("Package queue updated");
            });
            
            packageStage.setOnCloseRequest((WindowEvent window) -> {
                packageStage = null;
            });
        }
        
        // show stage and move to front
        packageStage.show();
        packageStage.toFront();
    }

    @FXML
    private void sendAllPackeges(ActionEvent event) {
        for (PostPackage pp : packageStorage.getUnsentPackages()) {
            try {

                System.out.println("Sending package " + pp);

                /* Must be better ways to check that travel distance for class 1 packages... */
                if (pp instanceof Class1Package) {
                    int distance = MapAPI.createPath(pp.getSendSmartPost(), pp.getReceiveSmartPost(), webmap, pp.getPackageClass(), "green");
                    pp.setTravelDistance(distance);
                    ((Class1Package) pp).send(distance);
                } else if (pp instanceof Class2Package) {
                    int distance = MapAPI.createPath(pp.getSendSmartPost(), pp.getReceiveSmartPost(), webmap, pp.getPackageClass(), "orange");
                    pp.setTravelDistance(distance);
                    pp.send();
                } else if (pp instanceof Class3Package) {
                    int distance = MapAPI.createPath(pp.getSendSmartPost(), pp.getReceiveSmartPost(), webmap, pp.getPackageClass(), "red");
                    pp.setTravelDistance(distance);
                    pp.send();
                }

                updatePackageQueue();
                updateSentPackageList();

            } catch (PackageException ex) {
                System.out.println("ERROR: " + ex.getMessage());
            }
        }
    }

    @FXML
    private void deleteDrawnRoutes(ActionEvent event) {
        MapAPI.deletePaths(webmap);
    }

    private void updatePackageQueue() {
        packageQueue.getItems().clear();

        for (PostPackage pp : packageStorage.getUnsentPackages()) {
            // mark travel distance for package
            pp.setTravelDistance(MapAPI.getDistanceBetweenPoints(pp.getSendSmartPost(), pp.getReceiveSmartPost(), webmap));

            // Add package to list
            packageQueue.getItems().add(pp);
        }

        // update label
        packageCountLabel.setText(packageStorage.getUnsentPackages().size() + " kpl");
    }

    private void updateSentPackageList() {
        sentPackagesList.getItems().clear();
        for (PostPackage pp : packageStorage.getSentPackages()) {

            sentPackagesList.getItems().add(pp);
        }

        // update label
        sentPackagesCountLabel.setText(packageStorage.getSentPackages().size() + " kpl");
    }

    /* Deletes all markers from the map */
    @FXML
    private void deleteAllMarkers(ActionEvent event) {
        MapAPI.removeAllMarkers(webmap);
        for (SmartPost sp : spc.getSmartPosts()) {
            sp.setInMap(false);
        }
    }

    /* Show custom tooltip for package */
    @FXML
    private void showPackageTooltip(MouseEvent event) {
        PostPackage pp = packageQueue.getSelectionModel().getSelectedItem();

        if (pp != null) {
            System.out.println();
            Tooltip t = new Tooltip(pp.getPackageClass() + ". luokan paketti\n\n"
                    + "Sisältö: " + pp.getItem().getName() + "\n"
                    + "Mistä: " + pp.getSendSmartPost() + "\n"
                    + "Minne: " + pp.getReceiveSmartPost() + "\n"
                    + "Tavaran koko: " + pp.getItem().getX() + "*" + pp.getItem().getY() + "*" + pp.getItem().getZ() + " cm\n"
                    + "Paino: " + pp.getItem().getWeight() + " kg\n"
                    + "Matkan pituus: " + pp.getTravelDistance() + " km\n"
                    + "Särkyvää: " + (pp.getItem().isBreakable() ? "Kyllä" : "Ei") + "");
            Node node = (Node) event.getSource();
            t.setAutoHide(true);
            t.show(node, event.getScreenX() - 2, event.getScreenY() - 2);

            tooltips.add(t);

        }
    }

    @FXML
    private void hidePackageTooltips(MouseEvent event) {
        for (Tooltip t : tooltips) {
            t.hide();
        }
        tooltips.clear();
        
        // Clear selection same time as tooltips
        packageQueue.getSelectionModel().clearSelection();
        sentPackagesList.getSelectionModel().clearSelection();

    }

    @FXML
    private void showSentPackageTooltip(MouseEvent event) {
        PostPackage pp = sentPackagesList.getSelectionModel().getSelectedItem();

        if (pp != null) {
            Tooltip t = new Tooltip(pp.getPackageClass() + ". luokan paketti\n\n"
                    + "Sisältö: " + pp.getItem().getName() + "\n"
                    + "Mistä: " + pp.getSendSmartPost() + "\n"
                    + "Minne: " + pp.getReceiveSmartPost() + "\n"
                    + "Tavaran koko: " + pp.getItem().getX() + "*" + pp.getItem().getY() + "*" + pp.getItem().getZ() + " cm\n"
                    + "Paino: " + pp.getItem().getWeight() + " kg\n"
                    + "Matkan pituus: " + pp.getTravelDistance() + " km\n"
                    + "Särkyvää: " + (pp.getItem().isBreakable() ? "Kyllä" : "Ei") + "\n"
                    + "Hajonnut: " + (pp.getItem().isBroken() ? "Kyllä" : "Ei") + "\n"
                    + "Huom: " + (pp.isTooFarAway() ? "Pakettia ei voitu lähettää. Matka on liian pitkä" : "Ei huomioita"));
            Node node = (Node) event.getSource();
            t.setAutoHide(true);
            t.show(node, event.getScreenX() - 2, event.getScreenY() - 2);

            tooltips.add(t);

        }
    }

    @FXML
    private void addCitySmartpostsToMap(ActionEvent event) {
        if (cityComboBox.getValue() == null) {
            System.out.println("Valitse Kaupunki");
        } else {
            spc.getSmartPosts(cityComboBox.getValue()).stream().forEach((sp) -> {
                if (!sp.isInMap()) {
                    MapAPI.addSmartPost(sp, webmap);
                    sp.setInMap(true);
                } else {
                    System.out.println("SmartPost on jo kartalla");
                }
            });
        }
    }

    @FXML
    private void showPackageStatistics(ActionEvent event) {
        try {
            // Allow only one window instance at a time
            if (statisticStage == null) {
                Parent statistics = FXMLLoader.load(getClass().getResource("StatisticsScene.fxml"));
                statisticStage = new Stage();
                Scene scene = new Scene(statistics);
                statisticStage.setResizable(false);
                statisticStage.setScene(scene);
                scene.getStylesheets().add(Main.class.getResource("StatisticsStyle.css").toExternalForm());
                
                statisticStage.setOnCloseRequest((WindowEvent window) -> {
                    statisticStage = null;
                });
            }
            
            // show stage and move to front
            statisticStage.show();
            statisticStage.toFront();
            
        } catch (IOException ex) {
            System.err.println(ex);
        }
    }
}
