
package timotei;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author matias
 */
public class PackageSerializer {
    public static final String PACKAGE_STORAGE_POSTFIX = "_PackageStorage.ser";
    
    public static void storeAllPackages() throws IOException {
        Date now = new Date();
        PackageStorage storage = PackageStorage.getInstance();
        File file = new File("storage/" + Log.LOGFILEDATEFORMAT.format(now) + PACKAGE_STORAGE_POSTFIX);
        file.getParentFile().mkdirs();
        try (ObjectOutputStream objectOut = new ObjectOutputStream(new FileOutputStream(file))) {
            objectOut.writeObject(storage.getPacketList());
            objectOut.flush();
        }
    }
    
    public static ArrayList<PostPackage> loadAllPackages(Date date) throws IOException {
        ArrayList<PostPackage> postPackages = new ArrayList<>();
        
        ObjectInputStream objectIn = new ObjectInputStream(new FileInputStream("storage/" + Log.LOGFILEDATEFORMAT.format(date) + PACKAGE_STORAGE_POSTFIX));
        try {
            postPackages = (ArrayList<PostPackage>) objectIn.readObject();
        } catch (ClassNotFoundException ex) {
            System.out.println("Invalid object");
            System.out.println(ex);
        }
        
        return postPackages;
    }
}
