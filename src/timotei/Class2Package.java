
package timotei;

/**
 *
 * @author matias
 */
public class Class2Package extends PostPackage {

    public Class2Package() {
        super(50, 50, 30);
        super.packageClass = 2;
        super.willBreak = false;
    }

    @Override
    public void send() throws PackageException {
        
        /* Break item if there is too much empty space in the box */
        double packageVolume = this.getSizeX() * this.getSizeY() * this.getSizeZ();
        double survivalChange = (this.getItem().getVolume() / packageVolume) + 0.4; // Add constant to reduce chance of breaking, because this is supposed to be the safest way of transport
        if (Math.random() > survivalChange && this.getItem().isBreakable()) {
            this.getItem().breakItem();
        }
        
        super.send();
    }

}
