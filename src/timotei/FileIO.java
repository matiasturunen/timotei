/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timotei;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author HessuMcMillan
 */
public class FileIO {
    
    public static void write(String outputFile, String input) throws IOException {
        
        // make directories if needed
        File file = new File(outputFile);
        file.getParentFile().mkdirs();
        
        
        try (BufferedWriter out = new BufferedWriter(new FileWriter(file))) {
            out.write(input);
        }
        
    }
    
    public static String read(String file) throws IOException{
             
        String text = "";
        String line;
        BufferedReader rFile = new BufferedReader(new FileReader(file));

        while(true){
            line = rFile.readLine();
            if(line == null){
                rFile.close();
                return text;
            }
            else{
                text += line+'\n';
            }
        }
    }
    
    public static String read(File file) throws IOException{
             
        String text = "";
        String line;
        BufferedReader rFile = new BufferedReader(new FileReader(file));

        while(true){
            line = rFile.readLine();
            if(line == null){
                rFile.close();
                return text;
            }
            else{
                text += line+'\n';
            }
        }
    }
    
    public static ArrayList<File> getFilesInFolder(String folderName, String filterString) {
        ArrayList<File> files = new ArrayList<>();
        
        FilenameFilter filter = (File file, String name) -> name.toLowerCase().endsWith(filterString);
        File folder = new File(folderName);
        File[] filesArr = folder.listFiles(filter);
        for (File f : filesArr) {
            files.add(f);
        }
        
        return files;
    }
    
}
