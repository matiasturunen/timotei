/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timotei;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author HessuMcMillan
 */
public class Log {
    
    public static final DateFormat LOGFILEDATEFORMAT = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss");
    
    public static void makeLog() throws IOException{
        PackageStorage ps = PackageStorage.getInstance();
	Date date = new Date();
	
        String log = "";
        log += "Varaston tilanne " + LOGFILEDATEFORMAT.format(date) + ":\n";
        log += "Paketteja varastossa: " + ps.getPacketCount() + " kpl\n";
        log += "Paketit: \n\n";
        
        for(PostPackage pp : ps.getPacketList() ){
            String packageInfo = pp.getPackageClass() + ". luokan paketti\n"
                    + "Sisältö: " + pp.getItem().getName() + "\n"
                    + "Mistä: " + pp.getSendSmartPost() + "\n"
                    + "Minne: " + pp.getReceiveSmartPost() + "\n"
                    + "Tavaran koko: " + pp.getItem().getX() + "*" + pp.getItem().getY() + "*" + pp.getItem().getZ() + " cm\n"
                    + "Paino: " + pp.getItem().getWeight() + " kg\n"
                    + "Särkyvää: " + (pp.getItem().isBreakable() ? "Kyllä" : "Ei") + "\n"
                    + "Lähetetty: " + (pp.isSent() ? "Kyllä" : "Ei") + "\n"
                    + "Matkan pituus: " + pp.getTravelDistance() + "\n"
                    + "Hajonnut: " + (pp.getItem().isBroken() ? "Kyllä" : "Ei");
            
            log += "Paketti:\n";
            log += packageInfo + "\n--------------------\n\n";
            
        }
        FileIO.write("logs/" + LOGFILEDATEFORMAT.format(date) + ".log", log);
    }
    
    public static ArrayList<PostPackage> parseLogFile(String filename) {
        ArrayList<PostPackage> packages = new ArrayList<>();
        
        
        return packages;
    }
    
}
