
package timotei;

/**
 *
 * @author matias
 */
public class Class3Package extends PostPackage {

    public Class3Package() {
        super(60, 60, 60);
        super.willBreak = false;
        super.packageClass = 3;
    }

    @Override
    public void send() throws PackageException {
        double r = Math.random();
        System.out.println("C3: " + r);
        if (Math.random() < 0.4 && this.getItem().isBreakable()) {
            // If this is enabled, all items will break for reasons unknown
            //this.getItem().breakItem();
        }
        
        super.send();
    }
}
