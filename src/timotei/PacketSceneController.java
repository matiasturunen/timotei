package timotei;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class PacketSceneController implements Initializable {

    private ItemStorage items = ItemStorage.getInstance();
    private SmartPostController spc = SmartPostController.getInstance();
    private PackageStorage packageStorage = PackageStorage.getInstance();

    @FXML
    private ComboBox<Item> itemsCombobox;
    @FXML
    private Button packetClassInfobtn;
    @FXML
    private Button cancelBtn;
    @FXML
    private Button makePacketBtn;
    @FXML
    private TextField itemNameField;
    @FXML
    private TextField itemSizeField;
    @FXML
    private TextField itemWeight;
    @FXML
    private RadioButton packetFirstClass;
    @FXML
    private RadioButton packetSecondClass;
    @FXML
    private RadioButton packetThirdClass;
    @FXML
    private ComboBox<String> startCitycmbBox;
    @FXML
    private ComboBox<String> endCitycmbBox;
    @FXML
    private CheckBox fragileCheckBox;
    @FXML
    private ComboBox<SmartPost> startCitySmartPost;
    @FXML
    private ComboBox<SmartPost> endCitySmartPost;
    @FXML
    private AnchorPane packetWindow;
    @FXML
    private ToggleGroup packetClass;
    
    private ArrayList<Tooltip> tooltips = new ArrayList<>();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        itemsCombobox.getItems().clear();
        
        Tooltip t = new Tooltip("Pakettien tiedot");
        t.setText((new PackageInformation()).getPacketInfo());
        packetClassInfobtn.setTooltip(t);
    }

    @FXML
    private void initItemSelector(Event event) {

        ArrayList<Item> itemList = items.getItemList();
        itemsCombobox.getItems().clear();
        System.out.println(itemList);

        for (Item item : itemList) {
            itemsCombobox.getItems().add(item);
        }

    }

    @FXML
    private void showPacketClassInfo(ActionEvent event) {

        PackageInformation info = new PackageInformation();
        Tooltip t = new Tooltip("Pakettien tiedot");
        t.setText(info.getPacketInfo());
        
        t.setAutoHide(true);
        
        Node node = (Node) event.getSource();
        Point2D p = node.localToScene(0.0, 0.0);
        t.show(node, 
                p.getX() + node.getScene().getX() + node.getScene().getWindow().getX(),
                p.getY() + node.getScene().getY() + node.getScene().getWindow().getY());
    }

    @FXML
    private void cancelPacket(ActionEvent event) {
        Stage stage = (Stage) cancelBtn.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void makeItem(ActionEvent event) {

        if (!(itemNameField.getText()).isEmpty() && !(itemSizeField.getText()).isEmpty()
                && !itemWeight.getText().isEmpty()) {

            /* Splitting the package dimensions from user input */
            String str = itemSizeField.getText();

            try {
                String[] size = str.split("[*]");

                /* Casting given values from Strings to Doubles */
                Double sizeX = Double.parseDouble(size[0]);
                Double sizeY = Double.parseDouble(size[1]);
                Double sizeZ = Double.parseDouble(size[2]);

                try {
                    Double weight = Double.parseDouble(itemWeight.getText());

                    /* Making new item */
                    Item packet = new Item(fragileCheckBox.selectedProperty().getValue(),
                            sizeX, sizeY, sizeZ, weight, itemNameField.getText());
                    items.addItem(packet);

                } catch (Exception e) {
                    showAlertError("Virheellinen valinta.", "Anna esineen massa numerona.");
                }

            } catch (Exception e) {
                showAlertError("Virheellinen valinta.", "Esineen koko tulee olla muotoa:\n"
                        + "\tluku*luku*luku");
            }

        }

    }

    @FXML
    private void initStartCitySelector(Event event) {
        startCitycmbBox.getItems().clear();
        startCitycmbBox.getItems().addAll(spc.getCitiesWithSmartPost());
    }

    @FXML
    private void initEndCitySelector(Event event) {
        endCitycmbBox.getItems().clear();
        endCitycmbBox.getItems().addAll(spc.getCitiesWithSmartPost());

    }

    @FXML
    private void initStartSmartPostSelector(Event event) {
        ArrayList<SmartPost> smartPosts = spc.getSmartPosts((String) startCitycmbBox.getValue());
        startCitySmartPost.getItems().clear();

        for (int i = 0; i < smartPosts.size(); i++) {
            startCitySmartPost.getItems().add(smartPosts.get(i));
        }

    }

    @FXML
    private void initEndSmartPostSelector(Event event) {
        ArrayList<SmartPost> smartPosts = spc.getSmartPosts((String) endCitycmbBox.getValue());
        endCitySmartPost.getItems().clear();

        for (int i = 0; i < smartPosts.size(); i++) {
            endCitySmartPost.getItems().add(smartPosts.get(i));
        }
    }

    @FXML
    private void makePacket(ActionEvent event) {

        if (startCitySmartPost.getValue() != null && endCitySmartPost.getValue() != null
                && itemsCombobox.getValue() != null) {

            PostPackage packet = null;
            if (packetFirstClass.isSelected()) {
                packet = new Class1Package();
            } else if (packetSecondClass.isSelected()) {
                packet = new Class2Package();
            } else if (packetThirdClass.isSelected()) {
                packet = new Class3Package();
            } else {
                System.out.println("Something went wrong.");
            }
            
            if (packet == null) {
                // Unknown error
                return;
            }

            packet.setSendSmartPost(startCitySmartPost.getValue());
            packet.setReceiveSmartPost(endCitySmartPost.getValue());
            
            if (!packet.setItem(itemsCombobox.getValue())) {
                showAlertError("Esine on liian suuri", "Valitsemasi esine ei mahdu valitsemaasi pakettiin.");
                return;
            }
            packageStorage.addPacket(packet);
            System.out.println("Luotiin uusi paketti: " + packet);

        } else {
            showAlertError("Virheellinen valinta.", "Valitse esine ja smartpostit ensin.");
        }
    }
    
    private void showAlertError(String title, String message) {
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle(title);
        alert.setContentText(message);
        alert.show();
    }
}
