
package timotei;

/**
 *
 * @author matias
 */
public class Class1Package extends PostPackage{
    
    /* Maximum distance this package can travel */
    private final int maxDistance = 150;

    public Class1Package() {
        super(60, 70, 40);
        super.packageClass = 1;
        super.willBreak = true;
    }
    
    public void send(double distance) throws PackageException {
        if (distance > maxDistance) {
            System.out.println("This package can't travel that far away");
            this.setIsTooFarAway();
            this.setSent();
            throw new PackageException("This package can't travel that far away");
        } else {
            super.send();
        }
    }

    @Override
    public void send() throws PackageException {
        throw new PackageException("Class 1 packages needs distance value");
    }
    
    
}
